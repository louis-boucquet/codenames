import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForOf, NgIf } from '@angular/common';

import { Card } from '../../model/card';

@Component({
	selector: 'div[app-board]',
	templateUrl: './board.component.html',
	styleUrls: ['./board.component.css'],
	standalone: true,
	imports: [NgForOf, NgIf],
})
export class BoardComponent {
	@Input()
	cards!: Card[];

	@Input()
	spectator = true;

	@Output()
	readonly selectWord = new EventEmitter<string>();
}
