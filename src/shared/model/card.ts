export const colors = ['neutral', 'blue', 'red', 'black'] as const;
export type Color = typeof colors[number];

export type Card = {
	word: string,
	color: Color,
	faceUp: boolean,
};
