import { TestBed } from '@angular/core/testing';
import { WordsService } from './words.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { firstValueFrom } from 'rxjs';
import { AutoEncoderService } from './auto-encoder.service';

let service: WordsService;
let http: HttpTestingController;

beforeEach(() => {
	TestBed.configureTestingModule({
		providers: [
			AutoEncoderService,
			WordsService,
		],
		imports: [
			HttpClientTestingModule,
		],
	});

	http = TestBed.inject(HttpTestingController);
	service = TestBed.inject(WordsService);
});

beforeEach(() => {
	service.words$.subscribe();
	http.expectOne({
		method: 'GET',
	}).flush('abc\ndef\nghi\n');
});

afterEach(() => {
	http.verify();
});

it('should get words', async () => {
	const words = await firstValueFrom(service.words$);

	expect(words.length).toBe(3);
});

it('should encode and decode words', async () => {
	const words = ['ghi', 'def'];

	const encoded = await firstValueFrom(service.encodeWords(words));
	const decoded = await firstValueFrom(service.decodeWords(encoded));

	expect(decoded).toEqual(words);
});
