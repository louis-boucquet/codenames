import { Injectable } from '@angular/core';
import { combineLatest, map, Observable, OperatorFunction, pipe, switchMap, tap } from 'rxjs';
import { Color } from '../shared/model/card';
import { ActivatedRoute } from '@angular/router';
import { WordsService } from './words.service';
import { ColorService } from './color.service';

@Injectable({
	providedIn: 'root',
})
export class BoardService {
	private readonly words$: Observable<string[]> = this.activatedRoute
		.queryParams
		.pipe(
			map(params => params['words']),
			switchMap(encodedWords => this.wordsService.decodeWords(encodedWords)),
			this.validateCardsArray(),
		);
	private readonly colors$: Observable<Color[]> = this.activatedRoute
		.queryParams
		.pipe(
			map(params => params['colors']),
			map(encodedColors => this.colorService.decodeColors(encodedColors)),
			this.validateCardsArray(),
		);
	readonly cards$ = combineLatest({
		words: this.words$,
		colors: this.colors$,
	}).pipe(
		map(({words, colors}) => words.map((word, i) => ({
			word,
			color: colors[i],
		}))),
	);

	constructor(
		private readonly activatedRoute: ActivatedRoute,
		private readonly wordsService: WordsService,
		private readonly colorService: ColorService,
	) {
	}

	private validateCardsArray(): OperatorFunction<any, any> {
		return pipe(
			tap(words => {
				if (!(Array.isArray(words) && words.length === 25))
					throw new Error(`Expected an array of 25 cards, but only found ${words.length}`);
			}),
		);
	}
}
