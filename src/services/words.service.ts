import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable, shareReplay, take } from 'rxjs';
import { AutoEncoderService } from './auto-encoder.service';

@Injectable({
	providedIn: 'root',
})
export class WordsService {
	readonly words$ = this.http
		.get(
			'https://raw.githubusercontent.com/Gullesnuffs/Codenames/master/wordlist-eng.txt',
			{responseType: 'text'},
		)
		.pipe(
			map(words => words
				.trim()
				.split('\n')
				.map(word => word.toLowerCase()),
			),
			shareReplay(1),
		);

	constructor(
		private readonly http: HttpClient,
		private readonly autoEncoderService: AutoEncoderService,
	) {
	}

	encodeWords(words: string[]): Observable<string> {
		return this.words$
			.pipe(
				take(1),
				map(wordList => this.autoEncoderService.encode(wordList, words)),
			);
	}

	decodeWords(wordsNumber: string): Observable<string[]> {
		return this.words$
			.pipe(
				take(1),
				map(wordList => this.autoEncoderService.decode(wordList, wordsNumber)),
			);
	}
}
