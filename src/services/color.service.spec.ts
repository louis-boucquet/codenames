import { TestBed } from '@angular/core/testing';

import { ColorService } from './color.service';
import { colors } from '../shared/model/card';

describe('ColorService', () => {
	let service: ColorService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(ColorService);
	});

	it('should be created', () => {
		expect(service.decodeColors(service.encodeColors(colors)))
			.toEqual(colors);
	});
});
