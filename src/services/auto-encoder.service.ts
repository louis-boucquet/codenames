import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class AutoEncoderService {

	encode<T>(
		sourceList: readonly T[],
		values: readonly T[],
	): string {
		let bigint = values
			.map(value => sourceList.indexOf(value))
			.map(BigInt)
			.reduce((tot, index) => tot * BigInt(sourceList.length) + index, 0n);

		return this.bigIntToString(bigint);
	}

	decode<T>(
		sourceList: readonly T[],
		bigIntString: string,
	): T[] {
		let valuesNumber = this.stringToBigInt(bigIntString);

		const numberBase = BigInt(sourceList.length);
		const values: T[] = [];

		while (valuesNumber > BigInt(0)) {
			values.push(sourceList[Number(valuesNumber % numberBase)]);
			valuesNumber /= numberBase;
		}

		return values.reverse();
	}

	private bigIntToString(bigint: bigint) {
		return this.hexToBase64(bigint.toString(16));
	}

	private hexToBase64(hex: string) {
		if (hex.length % 2 === 1)
			hex = `0${hex}`;

		return btoa(hex.match(/\w{2}/g)!
			.map(a => String.fromCharCode(parseInt(a, 16))).join(""));
	}

	private stringToBigInt(base64: string) {
		const hex = [...atob(base64)]
			.map(char => char.charCodeAt(0))
			.map(code => code.toString(16))
			.join('');

		return BigInt(`0x${hex}`);
	}
}
