import { TestBed } from '@angular/core/testing';

import { AutoEncoderService } from './auto-encoder.service';

let service: AutoEncoderService;

beforeEach(() => {
	TestBed.configureTestingModule({});
	service = TestBed.inject(AutoEncoderService);
});

it('should encode and decode words', () => {
	const source = ['abc', 'def', 'ghi'];
	const words = ['ghi', 'def'];

	const encoded = service.encode(source, words);
	const decoded = service.decode(source, encoded);

	expect(decoded).toEqual(words);
});
