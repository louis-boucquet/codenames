import { Injectable } from '@angular/core';
import { AutoEncoderService } from './auto-encoder.service';
import { Color, colors } from '../shared/model/card';

@Injectable({
	providedIn: 'root',
})
export class ColorService {
	constructor(
		private readonly autoEncoderService: AutoEncoderService,
	) {
	}

	encodeColors(colorsToEncode: readonly Color[]): string {
		return this.autoEncoderService.encode(colors, colorsToEncode);
	}

	decodeColors(colorsToEncode: string): Color[] {
		return this.autoEncoderService.decode(colors, colorsToEncode);
	}
}
