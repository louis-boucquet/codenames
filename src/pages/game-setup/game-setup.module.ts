import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameSetupComponent } from './game-setup.component';
import { RouterModule } from '@angular/router';
import { BoardComponent } from '../../shared/components/board/board.component';


@NgModule({
	declarations: [
		GameSetupComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: GameSetupComponent,
			},
		]),
		BoardComponent,
	],
})
export class GameSetupModule {
}
