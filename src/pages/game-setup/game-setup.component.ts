import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';

import { Card, Color } from '../../shared/model/card';
import { WordsService } from '../../services/words.service';
import { combineLatest, startWith, Subject } from 'rxjs';
import { ColorService } from '../../services/color.service';

@Component({
	selector: 'app-game-setup',
	templateUrl: './game-setup.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameSetupComponent {
	private readonly cards$ = new Subject<Card[]>();
	readonly viewModel$ = combineLatest({
		cards: this.cards$.asObservable()
			.pipe(startWith(null)),
	});

	constructor(
		private readonly router: Router,
		private readonly wordsService: WordsService,
		private readonly colorService: ColorService,
	) {
	}

	generateBoard() {
		this.wordsService.words$
			.subscribe(words => this.buildGame(words));
	}

	private buildGame(words: string[]) {
		const randomWords = this.getWords(words);
		const colors = this.getColors();

		this.cards$.next(
			randomWords.map((word, i) => ({
				word,
				color: colors[i],
				faceUp: false,
			})),
		);
	}

	startGame(cards: Card[]) {
		this.wordsService.encodeWords(cards
			.map(card => card.word))
			.subscribe(wordsEncoded => {
				this.router.navigate(['game', 'choice'], {
					queryParams: {
						words: wordsEncoded,
						colors: this.colorService.encodeColors(cards.map(card => card.color)),
					},
				});
			});
	}

	replaceWord(cards: Card[], word: string) {
		this.wordsService.words$
			.subscribe(words => {
				cards.find(card => card.word === word)!.word = words[Math.floor(Math.random() * words.length)];
				this.cards$.next(cards);
			});
	}

	private getWords(words: string[]): string[] {
		const randomWords = new Set<string>();

		while (randomWords.size < 25)
			randomWords.add(words[Math.floor(Math.random() * words.length)]);

		return [...randomWords];
	}

	private getColors(): Color[] {
		const colors: Color[] = [];

		for (let i = 0; i < 9; i++) colors.push('blue');
		for (let i = 0; i < 8; i++) colors.push('red');
		for (let i = 0; i < 7; i++) colors.push('neutral');
		colors.push('black');

		colors.sort(() => Math.random() - 0.5);

		return colors;
	}

	protected readonly console = console;
}
