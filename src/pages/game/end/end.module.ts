import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EndComponent } from './end.component';
import { RouterModule } from '@angular/router';


@NgModule({
	declarations: [
		EndComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: EndComponent,
			},
		]),
	],
})
export class EndModule {
}
