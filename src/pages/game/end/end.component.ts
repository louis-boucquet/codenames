import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs';

@Component({
	selector: 'app-end',
	templateUrl: './end.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EndComponent {
	readonly viewModel$ = this.activatedRoute
		.queryParams
		.pipe(map(params => params['color']));

	constructor(
		private readonly activatedRoute: ActivatedRoute,
	) {
	}
}
