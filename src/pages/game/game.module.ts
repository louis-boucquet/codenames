import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { RouterModule } from '@angular/router';

@NgModule({
	declarations: [
		GameComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: GameComponent,
				children: [
					{
						path: 'choice',
						loadChildren: () => import('./choice/choice.module')
							.then(m => m.ChoiceModule)
					},
					{
						path: 'controller',
						loadChildren: () => import('./controller-board/controller-board.module')
							.then(m => m.ControllerBoardModule),
					},
					{
						path: 'board',
						loadChildren: () => import('./playing-board/playing-board.module')
							.then(m => m.PlayingBoardModule),
					},
					{
						path: 'end',
						loadChildren: () => import('./end/end.module')
							.then(m => m.EndModule),
					},
				],
			},
		]),
	],
})
export class GameModule {
}
