import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BehaviorSubject, catchError, combineLatest, EMPTY, map } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { BoardService } from '../../../services/board.service';
import { Color } from '../../../shared/model/card';

@Component({
	selector: 'app-controller-board',
	templateUrl: './controller-board.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ControllerBoardComponent {
	private readonly selectedWords$ = new BehaviorSubject<Set<string>>(new Set());

	private readonly color$ = this.activatedRoute
		.queryParams
		.pipe(map(params => params['color']));

	private readonly cards$ = this.boardService.cards$.pipe(
		catchError(e => {
			alert(`Could not parse game: ${e.toString()}, redirecting to game setup`);
			this.router.navigate(['game-setup']);

			return EMPTY;
		}),
	);

	readonly viewModel$ = combineLatest({
		selectedWords: this.selectedWords$,
		cards: this.cards$,
		color: this.color$,
	}).pipe(
		map(({selectedWords, cards, color}) =>
			cards.map(card => ({
				...card,
				faceUp: selectedWords.has(card.word),
				color: this.mapCardColor(card.color, color),
			})),
		),
	);

	constructor(
		private readonly boardService: BoardService,
		private readonly router: Router,
		private readonly activatedRoute: ActivatedRoute,
	) {
	}

	selectWord(word: string) {
		this.selectedWords$.next(new Set([...this.selectedWords$.value, word]));
	}

	private mapCardColor(cardColor: Color, playerColor: 'blue' | 'red' | undefined): Color {
		if (playerColor === undefined)
			return cardColor;

		if (cardColor === playerColor)
			return playerColor;
		else return 'neutral';
	}
}
