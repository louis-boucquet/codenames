import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControllerBoardComponent } from './controller-board.component';
import { RouterModule } from '@angular/router';
import { BoardComponent } from '../../../shared/components/board/board.component';


@NgModule({
	declarations: [
		ControllerBoardComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: ControllerBoardComponent,
			},
		]),
		BoardComponent,
	],
})
export class ControllerBoardModule {
}
