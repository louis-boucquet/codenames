import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
	selector: 'app-game',
	template: '<router-outlet></router-outlet>',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameComponent {}
