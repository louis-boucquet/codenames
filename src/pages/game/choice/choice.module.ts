import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChoiceComponent } from './choice.component';
import { RouterModule } from '@angular/router';


@NgModule({
	declarations: [
		ChoiceComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: ChoiceComponent,
			},
		]),
	],
})
export class ChoiceModule {
}
