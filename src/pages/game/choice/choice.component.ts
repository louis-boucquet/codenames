import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
	selector: 'app-choice',
	templateUrl: './choice.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChoiceComponent {

}
