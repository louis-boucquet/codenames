import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayingBoardComponent } from './playing-board.component';
import { RouterModule } from '@angular/router';
import { BoardComponent } from '../../../shared/components/board/board.component';


@NgModule({
	declarations: [
		PlayingBoardComponent,
	],
	imports: [
		CommonModule,
		RouterModule.forChild([
			{
				path: '',
				component: PlayingBoardComponent,
			}
		]),
		BoardComponent,
	],
})
export class PlayingBoardModule {
}
