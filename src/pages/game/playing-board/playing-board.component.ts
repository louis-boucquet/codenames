import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { BehaviorSubject, catchError, combineLatest, EMPTY, map } from 'rxjs';
import { Router } from '@angular/router';
import { BoardService } from '../../../services/board.service';
import { Card, Color } from '../../../shared/model/card';

@Component({
	selector: 'app-playing-board',
	templateUrl: './playing-board.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayingBoardComponent implements OnDestroy {
	private readonly selectedWords$ = new BehaviorSubject<Set<string>>(new Set());

	private readonly cards$ = this.boardService.cards$.pipe(
		catchError(e => {
			alert(`Could not parse game: ${e.toString()}, redirectiong to game setup`);
			this.router.navigate(['game-setup']);

			return EMPTY;
		}),
	);

	readonly viewModel$ = combineLatest({
		selectedWords: this.selectedWords$,
		cards: this.cards$,
	}).pipe(
		map(({selectedWords, cards}) =>
			cards.map(card => ({
				...card,
				faceUp: selectedWords.has(card.word),
			})),
		),
	);

	constructor(
		private readonly boardService: BoardService,
		private readonly router: Router,
	) {
		this.viewModel$
			.subscribe(viewModel => this.checkGameFinished(viewModel));
	}

	ngOnDestroy() {
		this.selectedWords$.complete();
	}

	selectWord(word: string, cards: Card[]) {
		const color = cards
			.find(card => card.word === word)
			?.color;

		if (color === 'black')
			this.endGame('black');

		this.selectedWords$.next(new Set([...this.selectedWords$.value, word]));
	}

	private checkGameFinished(cards: Card[]) {
		const numBlue = this.countFaceUpColor(cards, 'blue');
		const numRed = this.countFaceUpColor(cards, 'red');

		if (numBlue === 9) this.endGame('blue');
		if (numRed === 8) this.endGame('red');
	}

	private endGame(blue: Color) {
		this.router.navigate(['game', 'end'], {
			queryParams: {
				color: blue,
			},
			queryParamsHandling: 'merge',
		});
	}

	private countFaceUpColor(faceUpCards: Card[], blue: Color) {
		return faceUpCards
			.filter(card => card.faceUp)
			.filter(card => card.color === blue)
			.length;
	}
}
