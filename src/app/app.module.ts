import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		RouterModule.forRoot([
			{
				path: 'game-setup',
				loadChildren: () => import('../pages/game-setup/game-setup.module')
					.then(m => m.GameSetupModule),
			},
			{
				path: 'game',
				loadChildren: () => import('../pages/game/game.module')
					.then(m => m.GameModule),
			},
			{
				path: '**',
				redirectTo: 'game-setup',
			},
		]),
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {
}
